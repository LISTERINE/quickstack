Quickstack
============

Full web stack in docker in minutes.

### Build your app

##### Run cookiecutter on the repository to generate the app base

```cookiecutter https://gitlab.com/LISTERINE/quickstack.git```

##### Enter the new project dir

``` cd {your project title} ```

##### Build and run the container

``` make dockerrun ```

##### Now browse to localhost

Enjoy your app!

### Technologies
* Docker/Docker-Compose
* Eventlet
* Flask
* Flask-Socketio
* Flask-Cache
* Flask-Compress
* Jquery
* Bootstrap
* Socketio

### Using without any SSL
set the ``` use_ssl ``` option to 'n', and the ``` lets_encrypt ``` to 'n'
That's it.

### Using SSL with let's encrypt
set the ``` use_ssl ``` option to 'y', and the ``` lets_encrypt ``` to 'y'
Add the generated "letsencrypt{app_name_here}.sh" to your crontab and set it to run monthly.
This will update your certs for you once a month to make sure they never expire.

### Using SSL without let's encrypt
set the ``` use_ssl ``` option to 'y', and the ``` lets_encrypt ``` to 'n'
Name your private key privkey.pem and place it here: <your-app-name>/ssl/privkey.pem
Name your certifiate chain file fullchain.pem and place it here: <your-app-name>/ssl/fullchain.pem
