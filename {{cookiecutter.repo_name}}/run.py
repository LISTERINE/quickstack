from app import create_app, socketio
import os
from flask_socketio import SocketIO
import eventlet
import logging

env = os.environ.get("APPNAME", "prod")
app = create_app(f'settings.{env.capitalize()}Config', env=env)

logging.basicConfig(filename="/var/log/flask.log", level=logging.DEBUG)


def run():
    socketio.run(app, host='0.0.0.0', port={{ cookiecutter.internal_port }})


if __name__ == "__main__":
    run()
