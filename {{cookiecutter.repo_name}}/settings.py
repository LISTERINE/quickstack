from os import path
import configparser

config = configparser.ConfigParser()


class Config(object):
    basedir = path.abspath(path.dirname(__file__))

    # mimetypes to compress
    COMPRESS_MIMETYPES = ['text/html', 'text/css', 'text/xml',
                          'application/json', 'application/javascript', 'image/svg+xml']

    SQLALCHEMY_DATABASE_URI = 'sqlite:///{{cookiecutter.repo_name}}.db'
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    {%- if cookiecutter.camera_stream.lower() == 'y' -%}
    IMAGES_DIR = path.join(basedir, "images")
    config.read(path.join(basedir, "config.ini"))
    API_KEY = config['{{cookiecutter.repo_name}}']['api_key']
    {% endif %}


class ProdConfig(Config):
    pass


class DevConfig(Config):
    pass
