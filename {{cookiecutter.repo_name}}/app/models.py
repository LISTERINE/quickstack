# coding: utf-8
from sqlalchemy import Column, Integer, String, Text
from flask_sqlalchemy import SQLAlchemy, Model


class SerializerMixin():

    def serialize(self):
        data = {}
        for k, v in self.__dict__.items():
            if k == "_sa_instance_state":
                continue
            elif isinstance(v, Model):
                data[k] = v.serialize()
            elif isinstance(v, list):
                data[k] = [getattr(sub_v, "serialize", lambda: sub_v)() for sub_v in v]
            elif isinstance(v, dict):
                data[k] = {sub_k: getattr(sub_v, "serialize", lambda: sub_v)() for sub_k, sub_v in v.items()}
            else:
                data[k] = v
        return data


db = SQLAlchemy()

if __name__ == "__main__":

    def create_all():
        db.create_all()
