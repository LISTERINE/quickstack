#  https://gist.github.com/whoiswes/64afe531a025c9b0b333
from marshmallow import fields
from marshmallow_sqlalchemy import ModelSchemaOpts
from marshmallow_sqlalchemy import ModelSchema
from app import models, ma, db


class BaseOpts(ModelSchemaOpts):
    def __init__(self, meta, ordered=False):
        if not hasattr(meta, "sqla_session"):
            meta.sqla_session = db.session
        super(BaseOpts, self).__init__(meta, ordered=ordered)


class BaseSchema(ModelSchema):
    OPTIONS_CLASS = BaseOpts
