from flask_assets import Environment
from webassets.loaders import PythonLoader as PythonAssetsLoader
from flask_caching import Cache
from flask_compress import Compress
# from flask_login import LoginManager
from flask_migrate import Migrate
from flask_socketio import SocketIO
from flask_marshmallow import Marshmallow
from app.models import db

from app import assets

# Import and register the different asset bundles
assets_env = Environment()
assets_loader = PythonAssetsLoader(assets)
cache = Cache(config={"CACHE_TYPE": "simple"})
compress = Compress()
# login_manager = LoginManager()
migrate = Migrate()
socketio = SocketIO()
ma = Marshmallow()


def register_extensions(app):
    migrate.init_app(app)
    assets_env.init_app(app)
    cache.init_app(app)
    compress.init_app(app)
    socketio.init_app(app)
    db.init_app(app)
    with app.app_context():
        db.create_all()
        db.session.commit()
        migrate.init_app(app, db)
        ma.init_app(app)
