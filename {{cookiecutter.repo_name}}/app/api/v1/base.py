from flask import Blueprint, jsonify, request, current_app
import json


base_api = Blueprint("base_api", __name__)


@base_api.route('/', methods=["POST"])
def index():
    return jsonify(success=True)
