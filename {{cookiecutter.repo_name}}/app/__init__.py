from flask import Flask, url_for
from flask.logging import default_handler
from .extensions import register_extensions
from .extensions import (assets_env, assets_loader,
                         cache, compress,
                         socketio,
                         migrate, ma, db)
from flask_assets import Environment
from webassets.loaders import PythonLoader as PythonAssetsLoader
from flask_socketio import SocketIO
{% if cookiecutter.camera_stream.lower() == 'y' -%}
from .camera import Camera{% endif %}


# Get static assets
from app import assets

from binascii import hexlify
from os import urandom
import logging
from logging.handlers import RotatingFileHandler

log_fmt = logging.Formatter('%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]')


# init socket context
def create_app(config, env="prod"):

    app = Flask(__name__)
    app.config.from_object(config)
    app.config["ENV"] = env
    app.config['SECRET_KEY'] = hexlify(urandom(80))

    register_extensions(app)
    # Import and register the different asset bundles
    for name, bundle in assets_loader.load_bundles().items():
        assets_env.register(name, bundle)

    if 'sqlite' in app.config['SQLALCHEMY_DATABASE_URI']:
        def _fk_pragma_on_connect(dbapi_con, con_record):  # noqa
            dbapi_con.execute('pragma foreign_keys=ON')

        with app.app_context():
            from sqlalchemy import event
            event.listen(db.engine, 'connect', _fk_pragma_on_connect)

    app.logger.removeHandler(default_handler)
    file_handler = RotatingFileHandler('/var/log/flask.log', 'a',
                                       1 * 1024 * 1024, 10)
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(log_fmt)
    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.DEBUG)
    stream_handler.setFormatter(log_fmt)
    app.logger.addHandler(file_handler)
    app.logger.addHandler(stream_handler)
    app.logger.setLevel(logging.DEBUG)
    app.logger.debug('site startup')

    # Register apps
    app.register_blueprint(base_view)

    # Register API routes
    app.register_blueprint(base_api, url_prefix="/api/v1")

    return app

from app.views.base import base_view
from app.api.v1.base import base_api
