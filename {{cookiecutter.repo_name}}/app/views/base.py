from flask import Blueprint, render_template
{% if cookiecutter.camera_stream.lower() == 'y' -%}
from flask import Response
{% endif %}
from flask_socketio import emit
from app import socketio, db
{% if cookiecutter.camera_stream.lower() == 'y' -%}
from app import Camera
from io import BytesIO
from dateutil import tz
{% endif %}


base_view = Blueprint("base_view", __name__)


@base_view.route('/')
@base_view.route('/index')
def index():
    return render_template("index.html")


@socketio.on('connect')
def joined(message=None):
    """Sent by clients when they request a connection. """
    emit('connect', {'data': 'connected'})


@socketio.on('clientHello')
def joined(message=None):
    """Sent by clients when they establish a full connection. """
    emit('serverHello', {'data': 'connected'})
{% if cookiecutter.camera_stream.lower() == 'y' -%}


def gen(camera):
    """Video streaming generator function."""
    while True:
        frame = camera.get_frame()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')


@base_view.route('/video_feed')
def video_feed():
    """Video streaming route. Put this in the src attribute of an img tag."""
    return Response(gen(Camera()),
                    mimetype='multipart/x-mixed-replace; boundary=frame')


@base_view.route('/frame')
def single():
    crop = [request.args.get("top", None),
            request.args.get("bottom", None),
            request.args.get("left", None),
            request.args.get("right", None)]
    if all(crop):
        try:
            crop_ints = [int(point) for point in crop]
            img = BytesIO(Camera().get_frame(crop=crop_ints))
        except Exception:
            return jsonify(success=False, error="invalid crop coord")
    else:
        img = BytesIO(Camera().get_frame())
    timestamp = datetime.utcnow().replace(tzinfo=tz.gettz('UTC'))
    try:
        timezone = request.args.get("tz")
        if timezone is not None:
            timestamp = timestamp.astimezone(tz.gettz(timezone)).timestamp()
    except Exception:
        return jsonify(success=False, error="invalid timezone: {}".format(timezone))
    return send_file(img, attachment_filename=str(timestamp)+".jpeg", as_attachment=True, cache_timeout=-1)
{% endif %}
