if (location.port) {
    var socket_port = location.port;
}
else {
    var socket_port = (location.protocol == "http:") ? 80 : 443;
}
var socket = io.connect(location.protocol + "//" + document.domain + ":" + socket_port);

socket.on("connect", function(msg) {
    console.log("got socket connect from server: ", msg);
    socket.emit("clientHello", {data: "I'm connected!"});
});

socket.on("serverHello", function(msg) {
    console.log("got socket status from server: ", msg, msg.data);
	$("#socketStatus").text(msg.data);
});
